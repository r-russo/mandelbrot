#ifndef MANDELBROT__DISPLAY_H_
#define MANDELBROT__DISPLAY_H_

#include <SDL2/SDL.h>
#include <vector>

int mandelbrot(int px, int py, int width, int height, int max_iter, double zoom, double offset_x, double offset_y);

class Display {
public:
    Display(int width, int height, int max_iter = 1000);
    ~Display();
    bool init_sdl();
    void main_loop();
    void update();
    void draw();
    void close();
private:
    void make_palette();

    SDL_Renderer *renderer {};
    SDL_Window *window {};

    int width, height;
    int max_iter;
    bool done {false}, dragging {false};
    double zoom {1};
    double offset_x {}, offset_y {};

    std::vector<SDL_Color> screen;
    std::vector<SDL_Color> palette;
};

#endif //MANDELBROT__DISPLAY_H_
