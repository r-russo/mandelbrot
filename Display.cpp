#include "Display.h"

int mandelbrot(int px, int py, int width, int height, int max_iter, double zoom, double offset_x, double offset_y) {
    double x0 = -2 + px * 3.0 / width;
    double y0 = -1 + py * 2.0 / height;
    x0 /= zoom;
    y0 /= zoom;
    x0 += offset_x * 3.0 / width;
    y0 += offset_y * 2.0 / height;
    double x = 0;
    double y = 0;
    double x2 = 0;
    double y2 = 0;

    int it = 0;

    while (it < max_iter && x2 + y2 <= 4) {
        y = 2 * x * y + y0;
        x = x2 - y2 + x0;
        x2 = x * x;
        y2 = y * y;

        it++;
    }

    return it;
}

Display::Display(int width, int height, int max_iter) {
    this->width = width;
    this->height = height;
    this->max_iter = max_iter;
    screen.resize(width * height);

    make_palette();

    if (!init_sdl()) {
        close();
    }
}

Display::~Display() {
    close();
}

bool Display::init_sdl() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        SDL_Log("Couldn't initialize video. Error: %s", SDL_GetError());
        return false;
    }

    window = SDL_CreateWindow("Mandelbrot", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height,
                              SDL_WINDOW_SHOWN);
    if (window == nullptr) {
        SDL_Log("Window couldn't be created. Error: %s", SDL_GetError());
        return false;
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    if (renderer == nullptr) {
        SDL_Log("Renderer couldn't be created. Error: %s", SDL_GetError());
        return false;
    }

    return true;
}

void Display::main_loop() {
    SDL_Event e;
    while (!done) {
        while (SDL_PollEvent(&e) != 0) {
            switch (e.type) {
                case SDL_QUIT:
                    done = true;
                    break;
                case SDL_MOUSEWHEEL:
                    if (e.wheel.y > 0) {
                        zoom *= 1.1;
                    } else if (e.wheel.y < 0) {
                        zoom /= 1.1;
                    }
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    dragging = true;
                    break;
                case SDL_MOUSEBUTTONUP:
                    dragging = false;
                    break;
                case SDL_MOUSEMOTION:
                    if (dragging) {
                        offset_x -= e.motion.xrel / zoom;
                        offset_y -= e.motion.yrel / zoom;
                    }
            }
        }

        update();
        draw();
    }
}

void Display::update() {
    int val;
    for (int x = 0; x < width; ++x) {
        for (int y = 0; y < height; ++y) {
            val = mandelbrot(x, y, width, height, max_iter, zoom, offset_x, offset_y);
            screen[y * width + x] = palette[val];
        }
    }
}

void Display::draw() {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);
    for (int i = 0; i < screen.size(); ++i) {
        SDL_SetRenderDrawColor(renderer, screen[i].r, screen[i].g, screen[i].b, SDL_ALPHA_OPAQUE);
        SDL_RenderDrawPoint(renderer, i % width, i / width);
    }
    SDL_RenderPresent(renderer);
}

void Display::close() {
    SDL_DestroyRenderer(renderer);
    renderer = nullptr;
    SDL_DestroyWindow(window);
    window = nullptr;
}

void Display::make_palette() {
    palette.resize(max_iter);
    for (int i = 0; i < palette.size(); ++i) {
        if (i < palette.size() / 3) {
            palette[i].r = (uint8_t) (i * 255.0 / static_cast<int>(palette.size() / 3));
            palette[i].g = 0;
            palette[i].b = 0;
        } else if (i < palette.size() * 2 / 3) {
            palette[i].r = 255;
            palette[i].g = (uint8_t) (static_cast<int>(i - palette.size() / 3) * 255.0
                    / static_cast<int>(palette.size() / 3));
            palette[i].b = 0;
        } else {
            palette[i].r = 255;
            palette[i].g = 255;
            palette[i].b = (uint8_t) (static_cast<int>(i - palette.size() * 2 / 3) * 255.0
                    / static_cast<int>(palette.size() / 3));
        }
        palette[i].a = SDL_ALPHA_OPAQUE;
    }
}
