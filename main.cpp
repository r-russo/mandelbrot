#include "Display.h"

int main() {
    const int width {640}, height {480};
    Display display(width, height, 100);

    display.main_loop();

    display.close();

    return 0;
}

